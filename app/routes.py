# app/routes.py

from app import app, REQUEST_COUNTER
from flask import render_template, request
from prometheus_client import generate_latest, CONTENT_TYPE_LATEST

@app.route('/')
def home():
    REQUEST_COUNTER.inc()  # Increment the counter
    user_ip = request.remote_addr
    return render_template('index.html', user_ip=user_ip)

@app.route('/metrics')
def metrics():
    return generate_latest(), 200, {'Content-Type': CONTENT_TYPE_LATEST}
