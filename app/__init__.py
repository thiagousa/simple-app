# app/__init__.py

from flask import Flask
from prometheus_client import Counter

app = Flask(__name__)

# Prometheus metrics
REQUEST_COUNTER = Counter('app_requests_total', 'Total number of requests')

from app import routes
