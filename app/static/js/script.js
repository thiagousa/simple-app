const images = [
    '/static/images/begin.jpg',
    '/static/images/park.jpg',
    '/static/images/arcade.jpg',
    '/static/images/elliot.jpg',
    '/static/images/robot.jpg',
    '/static/images/end.jpg',
    // ... more images
];

let currentImageIndex = 0;
let isBg2Visible = false;

function changeBackgroundImage() {
    // Increment the index first to get the next image
    currentImageIndex = (currentImageIndex + 1) % images.length;
    const nextImage = images[currentImageIndex];

    if (isBg2Visible) {
        document.getElementById('bg').style.backgroundImage = 'url(' + nextImage + ')';
        document.getElementById('bg').style.opacity = 1;
        document.getElementById('bg2').style.opacity = 0;
    } else {
        document.getElementById('bg2').style.backgroundImage = 'url(' + nextImage + ')';
        document.getElementById('bg2').style.opacity = 1;
        document.getElementById('bg').style.opacity = 0;
    }

    isBg2Visible = !isBg2Visible;
}

// Initial background image setup
document.addEventListener('DOMContentLoaded', function() {
    // Your code here
    document.getElementById('bg').style.backgroundImage = 'url(' + images[0] + ')';
    setInterval(changeBackgroundImage, 15000); // Change image every 30 seconds

});

